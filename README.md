The beginnings of the combat system for PokeMud.

The file of interest is:

commands/battle - The command which initiates the combat handler

typeclasses/combat_handler - Handling combat participants.

world/rules - Currently doesn't exist. Will handle combat resolution.