"""
Commands

Commands describe the input the player can do to the game.

"""

from evennia import Command
from evennia.utils.evmenu import get_input
from evennia import create_script
from typeclasses import characters
from evennia import CmdSet

# Alawys on trainers.

class CombatCmdSet(CmdSet):
    key = "combat_cmdset"
    mergetype = "Merge"
    priority = 10
    no_exits = True

    def at_cmdset_creation(self):
        self.add(CmdBattle())

class CmdBattle(Command):
    """
    initiates battle

    Usage:
      battle <trainer>[, <trainer>, <trainer>...]

    switch:
        /double
        /triple

    This will initiate combat with <trainer>. If <target is
    already in combat, you will join the combat.
    """
    key = "battle"
    help_category = "General"

    def func(self):
        "Handle command"
        if not self.args:
            self.caller.msg("Usage: battle <trainer>")
            return

        trainers = self.lhslist
        target = self.caller.search(self.args, typeclass=characters.Character)
        if not target:
            self.caller.msg("Usage: battle <trainer>")
            return

        # set up combat
        if target.ndb.combat_handler:
            self.caller.msg(target.key + " is already in battle.")
            return
        else:
            get_input(target, self.caller.key + " wants to fight! Do you accept?", battlecallback)
            self.caller.msg("You have challenged " + target.key + " to fight!")

def battlecallback(caller, prompt, user_input):
    """
    """
    challenger = caller.search(prompt.split()[0], typeclass=characters.Character)

    if user_input.lower() in ["yes", "y", "accept"]:
        caller.msg("You have accepted the challenge!")
        challenger.msg(caller.key + " has accepted your challenge!")

        chandler = create_script("typeclasses.combat_handler.CombatHandler")
        chandler.add_trainer(caller)
        chandler.add_trainer(challenger)

    else:
        caller.msg("You have declined to battle.")
        challenger.msg(caller.key + " has declined to battle.")


# Command available during Select phase of combat.

class SelectCmdSet(CmdSet):
    key = "InBattleCmdSet"
    mergetype = "Merge"
    priority = 10
    no_exits = True

    def at_cmdset_creation(self):
        self.add(CmdSelect())
        self.add(CmdRun())

class CmdSelect(Command):
    """
    Usage:
      select <pokemon>

    """
    key = "select"
    help_category = "General"

    def func(self):
        "Handle command"
        if not self.args:
            self.caller.msg("Usage: select <pokemon>")
            return

        pokemon = self.caller.search(self.args, typeclass="typeclasses.pokemon.Pokemon")

        if not pokemon:
            self.caller.msg("Usage: select <pokemon>")
            return

        if isinstance(pokemon, list):
            pokemon = pokemon[0]

        self.caller.ndb.combat_handler.add_pokemon(pokemon, self.caller)


class CmdRun(Command):
    """
    Usage:
      run

    """
    key = "run"
    help_category = "General"

    def func(self):
        "Handle command"
        self.caller.ndb.combat_handler.withdraw(self.caller)



# Given to each player for their pokemon.

class PokemonCommand(Command):
    """
    Allow player to call available actions.
    """
    obj = None

    def func(self):
        """
        move set.
        """
        self.obj.location.msg_contents("Test")
        # if "rock" in self.args:
        #     self.obj.ndb.combat_handler =
        #
        # if "return" in self.args:
        #     self.obj.location = None
        #
        # self.caller.msg("Test")