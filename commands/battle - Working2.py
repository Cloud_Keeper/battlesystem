"""
Commands

Commands describe the input the player can do to the game.

"""

from django.conf import settings
from evennia import Command
from evennia.utils.evmenu import get_input
from evennia import create_script
from typeclasses import characters
from evennia import CmdSet
from evennia.utils.utils import class_from_module

COMMAND_DEFAULT_CLASS = class_from_module(settings.COMMAND_DEFAULT_CLASS)


# Alawys on trainers.

class CombatCmdSet(CmdSet):
    key = "combat_cmdset"
    mergetype = "Merge"
    priority = 10
    no_exits = True

    def at_cmdset_creation(self):
        self.add(CmdBattle())


class CmdBattle(Command):
    """
    initiates battle

    Usage:
      battle <trainer>[, <trainer>, <trainer>...]

    switch:
        /double
        /triple

    This will initiate combat with <trainer>. If <target is
    already in combat, you will join the combat.
    """
    key = "battle"
    help_category = "General"

    def func(self):
        "Handle command"
        if not self.args:
            self.caller.msg("Usage: battle <trainer>")
            return

        target = self.caller.search(self.args,
                                    typeclass=characters.Character)
        if not target:
            self.caller.msg("Usage: battle <trainer>")
            return

        # set up combat
        if target.ndb.combat_handler:
            self.caller.msg(target.key + " is already in battle.")
            return
        else:
            get_input(target,
                      self.caller.key + " wants to fight! Do you accept?",
                      battlecallback)
            self.caller.msg(
                "You have challenged " + target.key + " to fight!")

# class CmdBattle(COMMAND_DEFAULT_CLASS):
#     """
#     Initiates battle
#
#     For a standard battle:
#         battle <trainer>
#
#     Advanced Usage:
#         battle[/switch] <trainer>, <trainer>:<trainer>, <trainer>:...]
#
#     For 2 teams of 2 trainers, 2 pokemon each with 2 reserves.
#         battle/f2/t4 <trainer>: <trainer>:<trainer>
#
#     switch:
#         /f2-6 : Field up to 6 pokemon (1 by defualt).
#         /t5-1 : Play with down to 1 pokemon (6 by default).
#         # Can double up switches "battle/5/3 trainer"
#         Consider for pokemon out at a time and team size.
#     """
#     key = "battle"
#     help_category = "General"
#
#     def func(self):
#         """
#         Handle command
#         """
#         caller = self.caller
#
#         if not self.args:
#             caller.msg("View 'help battle' for more information.")
#             return
#
#         if len(self.lhslist) > 3:
#             caller.msg("Testing")
#             return
#
#         trainers = []
#         for obj in self.lhslist:
#
#             trainer = caller.search(obj, typeclass=characters.Character)
#
#             if not trainer:
#                 caller.msg(trainer.key + " could not be found. Battle aborted.")
#                 return
#
#             if trainer in trainers or trainer == caller:
#                 caller.msg(trainer.key + " doubled up. Battle aborted.")
#                 return
#
#             if trainer.ndb.combat_handler:
#                 caller.msg(trainer.key + " is already in battle. Battle aborted.")
#                 return
#
#             trainers.append(trainer)
#
#         switch = self.switches[0] if self.switches else 1
#         pokenumber = switch if int(switch) in [1,2,3,4,5,6] else 1
#         trainerlist = ", ".join(trainer.key for trainer in trainers)
#         challenge = "%s wants to fight!\nThe battle is between %s and %s.\nEach trainer will field %s Pokemon at at time.\nDo you accept?"
#
#         caller.msg(challenge % (caller.key, trainerlist, caller.key, str(pokenumber)))
#
#         # for trainer in trainers:
#         #     get_input(trainer, challenge, battlecallback)
#         #     self.caller.msg("You have challenged " + trainerlist " to fight!")

def battlecallback(caller, prompt, user_input):
    """
    """
    challenger = caller.search(prompt.split()[0], typeclass=characters.Character)

    if user_input.lower() in ["yes", "y", "accept"]:
        caller.msg("You have accepted the challenge!")
        challenger.msg(caller.key + " has accepted your challenge!")

        chandler = create_script("typeclasses.combat_handler.CombatHandler")
        chandler.add_trainer(caller)
        chandler.add_trainer(challenger)

    else:
        caller.msg("You have declined to battle.")
        challenger.msg(caller.key + " has declined to battle.")


# Available all the way through combat.


class InBattleCmdSet(CmdSet):
    key = "InBattleCmdSet"
    mergetype = "Merge"
    priority = 10
    no_exits = True

    def at_cmdset_creation(self):
        self.add(CmdRun())

class CmdRun(Command):
    """
    Usage:
      run

    """
    key = "run"
    help_category = "General"

    def func(self):
        "Handle command"
        self.caller.ndb.combat_handler.withdraw(self.caller)


# Command available during Select phase of combat.

class SelectCmdSet(CmdSet):
    key = "SelectCmdSet"
    mergetype = "Merge"
    priority = 10
    no_exits = True

    def at_cmdset_creation(self):
        self.add(CmdChoose())


class CmdChoose(Command):
    """
    Usage:
      select <pokemon>

    """
    key = "choose"
    help_category = "General"

    def func(self):
        "Handle command"
        if not self.args:
            self.caller.msg("Usage: select <pokemon>")
            return

        pokemon = self.caller.search(self.args, typeclass="typeclasses.pokemon.Pokemon")

        if not pokemon:
            self.caller.msg("Usage: select <pokemon>")
            return

        if isinstance(pokemon, list):
            pokemon = pokemon[0]

        self.caller.ndb.combat_handler.add_pokemon(pokemon, self.caller)



# Given to each player for their pokemon.

class PokemonCommand(Command):
    """
    Allow player to call available actions.
    """
    obj = None

    def func(self):
        """
        move set.
        """
        if self.args.strip().lower() in ("rock", "paper", "scissors"):
            self.caller.ndb.combat_handler.add_action(self.args.strip().lower(), self.obj, self.caller)
        else:
            self.caller.msg("Valid action not entered.")
