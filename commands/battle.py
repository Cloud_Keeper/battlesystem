# -*- coding: utf-8 -*-
"""
Commands

Commands describe the input the player can do to the game.


NOTES:
    the script can do the confirming.

first stage - send inputs out to everyone. stage doesn't start unless everyone yes or aborts
second stage - everyone chooses pokemon until conditions are met.
third stage - battle

"""
from django.conf import settings
from evennia import Command
from evennia.utils import evtable, evmenu
from evennia import create_script
from typeclasses import characters
from evennia import CmdSet
from evennia.utils.utils import class_from_module

COMMAND_DEFAULT_CLASS = class_from_module(settings.COMMAND_DEFAULT_CLASS)

#########################
# Always Available Commands
#########################


class CombatCmdSet(CmdSet):
    key = "combat_cmdset"
    mergetype = "Merge"
    priority = 10
    no_exits = True

    def at_cmdset_creation(self):
        self.add(CmdBattle())


class CmdBattle(COMMAND_DEFAULT_CLASS):
    """
    Initiates battle

    For a standard battle:
        battle <trainer>

    Advanced Usage:
        battle[/switch] <trainer>:<trainer>, <trainer>:...]

    For 2 teams of 2 trainers, 2 pokemon each with 2 reserves.
        battle/f2/t4 <trainer>:<trainer>, <trainer>:<trainer>, <trainer>
        battle/f2/t4 :<trainer>:<trainer>

    switch:
        /f# : # Number of pokemon to field. Max 6 pokemon (1 by defualt).
        /p# : # Number of pokemon in party. Minimum 1 pokemon (6 by default).
        /builder : Starts battle builder evmenu
        # Can double up switches "battle/5/3 trainer"
        Consider for pokemon out at a time and team size.

    #TO DO BETS.
    """
    key = "battle"
    help_category = "General"

    def func(self):
        """
        Handle command
        """
        caller = self.caller

        # Default Values
        trainers = []
        teams = []
        party_number = 6
        field_number = 1

        # Accept party and field numbers. TODO CAN"T DO A VALUE START BUILDER
        for value in self.switches:
            party_number = int(value[1]) if (value[0] == "p" and int(value[1]) > 0 and int(value[1]) < 7) else party_number
            field_number = int(value[1]) if (value[0] == "f" and int(value[1]) > 0 and int(value[1]) <= party_number) else field_number

        # Accept trainers.
        teams = caller.key + "," + self.args if self.args else caller.key
        teams = teams.split(":")
        for i in xrange(len(teams)):
            teams[i] = teams[i].split(",")
            for trainer in teams[i]:
                # Catch empty strings.
                if not trainer:
                    teams[i].remove(trainer)
                    continue

                trainer = caller.search(trainer.strip(),
                                        typeclass=characters.Character,
                                        exact=True)

                if not trainer:
                    caller.msg(str(trainer) + " could not be found. Battle aborted.")
                    return

                if trainer.ndb.combat_handler:
                    caller.msg(trainer.key + " is already in battle. Battle aborted.")
                    return

                if trainer in trainers:
                    caller.msg(trainer.key + " doubled up. Battle aborted.")
                    return

                trainers.append(trainer)

        # Catch 1v1 battles.
        if len(teams) == 1 and len(teams[0]) > 1:
            teams.append([teams[0].pop(0)])

        # Start Battle if able and builder not specifically called.
        if len(teams) > 1 and "builder" not in self.switches:
            battledict = {
                "trainers": trainers,
                "teams": teams,
                "party_number": party_number,
                "field_number": field_number,
            }
            chandler = create_script("typeclasses.combat_handler.CombatHandler")
            chandler.begin_challenge(battledict)
            return

        # Start Builder with values
        evmenu.EvMenu(caller, "commands.battle",
                      startnode="battle_builder_node", persistent=False, # Change persistent to True
                      cmdset_mergetype="Replace",
                      node_formatter=battle_node_formatter,
                      trainers=trainers,
                      teams=teams,
                      party_number=party_number,
                      field_number=field_number)
        return

def battle_node_formatter(nodetext, optionstext, caller=None):
    """
    A minimalistic node formatter, no lines or frames.
    """
    return nodetext + "\n" + optionstext


def battle_options_formattter(optionlist, caller=None):
    """
    A minimalistic options formatter that mirrors a rooms content list.
    """
    if not optionlist:
        return ""

    return "You see: " + ", ".join([key for key, msg in optionlist])


def battle_builder_node(caller, input_string):
    # Set up variables
    menu = caller.ndb._menutree

    # Parse input
    #party/add *
    #party/remove *

    # Draw table.
    table = evtable.EvTable("Battle Builder", border="table")
    table.reformat(corner_char="◓", border_char="═",
                   border_left_char=" ", border_right_char=" ")

    for i in xrange(len(menu.teams)):
        table.add_row("Team " + str(i + 1) + ": " + ", ".join(str(trainer) for trainer in menu.teams[i]) + ".")
    table.add_row("Rules:")
    table.add_row("Party Size = " + str(menu.party_number))
    table.add_row("Combatants = " + str(menu.field_number))

    # Set up return options.
    text = table
    options = [{"key": "_default",
                "goto": "battle_builder_node"}]

    return text, options

#########################
# Commands available throughout battles.
#########################


class InBattleCmdSet(CmdSet):
    key = "InBattleCmdSet"
    mergetype = "Merge"
    priority = 10
    no_exits = True

    def at_cmdset_creation(self):
        self.add(CmdRun())

class CmdRun(Command):
    """
    Usage:
      run

    """
    key = "run"
    help_category = "General"

    def func(self):
        "Handle command"
        self.caller.ndb.combat_handler.withdraw(self.caller)


#########################
# Commands available in Select Phase
#########################

class SelectCmdSet(CmdSet):
    key = "SelectCmdSet"
    mergetype = "Merge"
    priority = 10
    no_exits = True

    def at_cmdset_creation(self):
        self.add(CmdChoose())


class CmdChoose(COMMAND_DEFAULT_CLASS):
    """
    Usage:
      select <pokemon>

    """
    key = "choose"
    help_category = "General"

    def func(self):
        "Handle command"
        if not self.args:
            self.caller.msg("Usage: select <pokemon>")
            return

        pokemon = self.caller.search(self.args, typeclass="typeclasses.pokemon.Pokemon")

        if not pokemon:
            self.caller.msg("Usage: select <pokemon>")
            return

        if isinstance(pokemon, list):
            pokemon = pokemon[0]

        self.caller.ndb.combat_handler.add_pokemon(pokemon, self.caller)

#########################
# Commands available in Action Phase
#########################


class PokemonCommand(COMMAND_DEFAULT_CLASS):
    """
    Pokemon [use] <Action> [on] <Target> [= Message]
    """
    obj = None

    def func(self):
        """
        move set.
        """
        caller = self.caller
        pokemondict = caller.ndb.combat_handler.db.pokemon

        if not self.args:
            caller.msg("Tell Pokemon to do what?")
            return

        text = self.args.lower().split()
        for int in xrange(len(text)):
            if text[int] in ["use", "on"]:
                text.pop(int)

        if len(text) > 2:
            caller.msg("Usage: Pokemon [use] <Action> [on] <Target>")
            return

        # Handle Target
        if len(text) == 2:
            target = self.caller.search(text[1],
                                        typeclass="typeclasses.pokemon.Pokemon",
                                        candidates=pokemondict.keys())
            if not target:
                caller.msg("Invalid target entered")
                return
            else:
                # Check for teams
                self.obj.ndb.target = target

        if not self.obj.ndb.target:
            caller.msg("Must specify a target.")
            return

        # Handle Action
        if text[0] not in ["rock", "paper", "scissors"]:
            caller.msg("Invalid action entered")
            return

        self.caller.ndb.combat_handler.add_action(text[0], self.obj, self.caller)
