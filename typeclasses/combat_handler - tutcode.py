import random
from evennia import DefaultScript
from evennia.commands import cmdset
from commands.battle import PokemonCommand

class CombatHandler(DefaultScript):
    """
    This implements the combat handler.

    Phases:
        Select
        Action
    """

    # standard Script hooks 

    def at_script_creation(self):
        "Called when script is first created"

        self.key = "combat_handler_%i" % random.randint(1, 1000)
        self.desc = "handles combat"
        self.interval = 60 * 2  # two minute timeout
        self.start_delay = True
        self.persistent = True   


        self.db.phase = "Select"
        # store all combatants
        self.db.trainers = {}
        self.db.pokemon = {}


    def at_start(self):
        """
        This is called on first start but also when the script is restarted
        after a server reboot. We need to re-assign this combat handler to
        all characters as well as re-assign the cmdset.
        """
        if self.db.phase == "Select":
            for trainer in self.db.trainers.keys():
                self._init_trainer_select(trainer)

        if self.db.phase == "Action":
            for trainer in self.db.trainers.keys():
                self._init_trainer_action(trainer)

    def _init_trainer_select(self, trainer):
        """
        This initializes handler back-reference 
        and combat cmdset on a trainer
        """
        trainer.ndb.combat_handler = self

        if self.db.phase == "Select":
            trainer.cmdset.add("commands.battle.SelectCmdSet")

    def _init_trainer_action(self, trainer):
        """
        Creates pokemon commands during combat.
        """

        trainer.ndb.combat_handler = self

        pokemon = self.db.trainers[trainer]
        cmd = PokemonCommand(key=pokemon.key.lower(),
                             aliases=pokemon.aliases.all(),
                             obj=pokemon)
        # create a cmdset
        poke_cmdset = cmdset.CmdSet(None)
        poke_cmdset.key = pokemon.key + 'CmdSet'
        poke_cmdset.priority = 101
        poke_cmdset.mergetype = "Replace"
        poke_cmdset.duplicates = True

        poke_cmdset.add(cmd)
        trainer.cmdset.add(poke_cmdset)



    # Called by the Battle command.

    def add_trainer(self, trainer):
        "Add combatant to handler"
        # set up back-reference
        self._init_trainer_select(trainer)

        self.db.trainers[trainer] = []
        trainer.msg("Choose your Pokemon!")




    # Called by the Select command.

    def add_pokemon(self, pokemon, trainer):
        """
        Add pokemon to trainers list and action list.
        """
        if not self.db.trainers[trainer]:
            self.db.trainers[trainer] = pokemon
            self.db.pokemon[pokemon] = None
            pokemon.ndb.combat_handler = self
            trainer.msg("You have selected a "+pokemon.key+" for battle.")
            self.check_end_selection()
        else:
            trainer.msg("You have already selected a Pokemon")

    def check_end_selection(self):
        """
        If everyone has picked a pokemon move to the next stage.
        """
        if all(trainer for trainer in self.db.trainers.values()):
            self.ndb.selection_turn = True
            self.db.phase = "Action"
            self.force_repeat()





    def at_repeat(self):
        """
        This is called every self.interval seconds (turn timeout) or 
        when force_repeat is called (because everyone has entered their 
        commands). We know this by checking the existence of the
        `normal_turn_end` NAttribute, set just before calling 
        force_repeat.

        """
        if self.ndb.selection_turn:
            self.msg_all("Everyone selected a Pokemon.")
            del self.ndb.selection_turn
            for trainer in self.db.trainers.keys():
                self._init_trainer_action(trainer)

        else:        
            # turn timeout
            self.msg_all("Combat has ended due to inaction.")
            self.stop()

    def withdraw(self, trainer):
        self.msg_all(trainer.key + " has withdrawn from the battle.", exceptions=[trainer])
        trainer.msg("You have withdrawn from the battle.")
        self.stop()

    def msg_all(self, message, exceptions=[]):
        "Send message to all combatants"
        for trainer in self.db.trainers.keys():
            if trainer not in exceptions:
                trainer.msg(message)





    def at_stop(self):
        """
        Called just before the script is stopped/destroyed.
        """
        for trainer in list(self.db.trainers.keys()):
            # note: the list() call above disconnects list from database
            self._cleanup_trainer(trainer)
        for pokemon in list(self.db.pokemon.keys()):
            # note: the list() call above disconnects list from database
            self._cleanup_pokemon(pokemon)

    def _cleanup_trainer(self, trainer):
        """
        Remove character from handler and clean
        it of the back-reference and cmdset
        """

        if self.db.phase == "Select":
            trainer.cmdset.delete("commands.battle.SelectCmdSet")

        if self.db.phase == "Action":
            pokemon = self.db.trainers[trainer]
            trainer.cmdset.delete(pokemon.key+"CmdSet")

        del self.db.trainers[trainer]
        del trainer.ndb.combat_handler


    def _cleanup_pokemon(self, pokemon):
        """
        Remove character from handler and clean
        it of the back-reference and cmdset
        """
        del self.db.pokemon[pokemon]
        del pokemon.ndb.combat_handler

