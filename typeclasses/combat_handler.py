# -*- coding: utf-8 -*-

import random
from evennia import DefaultScript
from evennia.commands import cmdset
from commands.battle import PokemonCommand
from evennia.utils.evmenu import get_input
from evennia.utils import evtable, evmenu
import itertools

class CombatHandler(DefaultScript):
    """
    This implements the combat handler.

    Phases:
        Select
        Action
    """
    #########################
    # At Script Creation
    #########################

    def at_script_creation(self):
        "Called when script is first created"

        self.key = "combat_handler_%i" % random.randint(1, 1000)
        self.desc = "handles combat"
        self.interval = 60 * 2  # two minute timeout
        self.start_delay = True
        self.persistent = True   

        # Battle Parameters. trainers, teams, party_number, field_number
        self.db.battledict = {}
        # Script State: Challenge / Select / Action
        self.db.phase = "Challenge"
        # Trainers: Holds challenge/select/battle choices.
        self.db.trainers = {}
        self.db.pokemon = {}

    def at_start(self):
        """
        This is called on first start but also when the script is restarted
        after a server reboot. We need to re-assign this combat handler to
        all characters as well as re-assign the cmdset.
        """
        if self.db.phase == "Challenge" and self.db.battledict:
            self.db.trainers = dict.fromkeys(self.db.battledict["trainers"])
            for trainer in self.db.trainers.keys():
                self._init_trainer_challenge(trainer)

        if self.db.phase == "Select":
            for trainer in self.db.trainers.keys():
                self._init_trainer_select(trainer)

        if self.db.phase == "Action":
            for trainer in self.db.trainers.keys():
                self._init_trainer_action(trainer)

    def at_repeat(self):
        """
        This is called every self.interval seconds (turn timeout) or
        when force_repeat is called (because everyone has entered their
        commands). We know this by checking the existence of the
        `normal_turn_end` NAttribute, set just before calling
        force_repeat.

        """
        if self.ndb.challenge_turn:
            self.msg_all("Choose your Pokemon!")
            del self.ndb.challenge_turn
            for trainer in self.db.trainers.keys():
                self.db.trainers[trainer] = [None] * self.db.battledict["field_number"]
                self._init_trainer_select(trainer)

        elif self.ndb.selection_turn:
            self.msg_all("Everyone selected a Pokemon.")
            del self.ndb.selection_turn
            for trainer in self.db.trainers.keys():
                trainer.cmdset.delete("SelectCmdSet")
                trainer.msg("Select an action!")
                self._init_trainer_action(trainer)

        elif self.ndb.action_turn:
            self.msg_all("Everyone selected an action.")
            del self.ndb.action_turn
            self.action_resolution(self.db.pokemon)
            self.msg_all("Select an action!")
            self.db.pokemon = dict.fromkeys(self.db.pokemon, None)

        else:
            # turn timeout
            self.msg_all("Combat has ended due to inaction.")
            self.stop()

    #########################
    # Challenge stage
    #########################

    def begin_challenge(self, battledict):
        """
        Receive battle dictionary. A challenge is sent out to participants
        to accept or not accept.
        """
        self.db.battledict = battledict
        self.at_start()

    def _init_trainer_challenge(self, trainer):
        """

        """
        trainer.ndb.combat_handler = self
        trainer.cmdset.add("commands.battle.InBattleCmdSet")

        # Draw table from battledict.
        battledict = self.db.battledict
        table = evtable.EvTable("Challenge!", border="table")
        table.reformat(corner_char="◓", border_char="═",
                       border_left_char=" ", border_right_char=" ")

        for i in xrange(len(battledict["teams"])):
            table.add_row("Team " + str(i + 1) + ": " + ", ".join(
                str(trainer) for trainer in battledict["teams"][i]) + ".")
        table.add_row("Rules:")
        table.add_row("Party Size = " + str(battledict["party_number"]))
        table.add_row("Combatants = " + str(battledict["field_number"]))

        get_input(trainer, str(table), self.challenge_callback)


    def challenge_callback(self, caller, prompt, user_input):
        """

        """
        trainerlist = dict(self.db.trainers)
        trainerlist.pop(caller)

        if user_input.lower() in ["yes", "y", "accept"]:
            self.db.trainers[caller] = True

            # Inform participants.
            caller.msg("You have accepted the battle!")
            if any(trainerlist.values()):
                caller.msg(", ".join(trainer.key for trainer, key in trainerlist.iteritems() if key) + " has also accepted the battle.")
                for trainer, key in trainerlist.iteritems():
                    if key:
                        trainer.msg(caller.key + " has accepted to battle.")
            if not any(trainerlist.values()):
                caller.msg(", ".join(trainer.key for trainer, key in trainerlist.iteritems() if not key) + " has yet to respond.")

            # Trigger Select Phase
            if all(trainerlist.values()):
                self.ndb.challenge_turn = True
                self.db.phase = "Select"
                self.force_repeat()

        else:
            caller.msg("You have declined to battle.")
            self.msg_all(caller.key + " has declined from the battle.",
                         exceptions=[caller])
            self.stop()  # TODO CANCEL OTHER PEOPLES INPUT MENUS

    #########################
    # Select stage
    #########################

    def _init_trainer_select(self, trainer):
        """
        This initializes handler back-reference 
        and combat cmdset on a trainer
        """
        trainer.ndb.combat_handler = self

        trainer.cmdset.add("commands.battle.InBattleCmdSet")
        trainer.cmdset.add("commands.battle.SelectCmdSet")

    # Called by the Select command.

    def add_pokemon(self, pokemon, trainer):
        """
        Add pokemon to trainers list and action list.
        """
        # TODO CHECK IF POKEMON ALREADY IN BATTLE
        if None in self.db.trainers[trainer]:
            for i in xrange(len(self.db.trainers[trainer])):
                if not self.db.trainers[trainer][i]:
                    self.db.trainers[trainer][i] = pokemon
                    break
            self.db.pokemon[pokemon] = None
            pokemon.ndb.combat_handler = self
            trainer.msg("You have selected a "+pokemon.key+" for battle.")
            self.check_end_selection()
        else:
            trainer.msg("You have already selected a Pokemon")

    def check_end_selection(self):
        """
        If everyone has picked a pokemon move to the next stage.
        """
        pokemonlist = itertools.chain.from_iterable(self.db.trainers.values())
        if all(pokemonlist):
            self.ndb.selection_turn = True
            self.db.phase = "Action"
            self.force_repeat()

    #########################
    # Action stage
    #########################

    def _init_trainer_action(self, trainer):
        """
        Creates pokemon commands during combat.
        """

        trainer.ndb.combat_handler = self
        trainer.cmdset.add("commands.battle.InBattleCmdSet")

        # create a cmdset
        actioncmdset = cmdset.CmdSet(None)
        actioncmdset.key = 'ActionCmdSet'
        actioncmdset.priority = 101
        actioncmdset.mergetype = "Replace"
        actioncmdset.duplicates = True

        for pokemon in self.db.trainers[trainer]:
            cmd = PokemonCommand(key=pokemon.key.lower(),
                                 aliases=pokemon.aliases.all(),
                                 obj=pokemon)
            actioncmdset.add(cmd)
        trainer.cmdset.add(actioncmdset)


    def add_action(self, action, pokemon, trainer):
        """
        Add pokemon to trainers list and action list.
        """
        if not self.db.pokemon[pokemon]:
            self.db.pokemon[pokemon] = action
            trainer.msg(pokemon.key+" will do a " + action + " attack.")
            self.check_end_action()
        else:
            trainer.msg("You have already selected an action for that Pokemon")

    def check_end_action(self):
        """
        If everyone has picked an action move to the next stage.
        """
        if all(self.db.pokemon.values()):
            self.ndb.action_turn = True
            self.db.phase = "Action"
            self.force_repeat()

    # This is normally done in a rules module.
    def action_resolution(self, pokedict):
        """

        """
        for pokemon in pokedict.keys():
            target = pokemon.ndb.target

            if pokedict[pokemon] == pokedict[target]:
                self.msg_all(pokemon.key + " uses Rock on " + target.key + ". It's a tie!")

            elif pokedict[pokemon] == "rock":
                if pokedict[target] == "paper":
                    self.msg_all(pokemon.key + " uses Rock on " + target.key + ". It's not very effective!")

                else:
                    self.msg_all(pokemon.key + " uses Rock on " + target.key + ". It's super effective!")

            elif pokedict[pokemon] == "paper":
                if pokedict[target] == "scissors":
                    self.msg_all(pokemon.key + " uses Paper on " + target.key + ". It's not very effective!")

                else:
                    self.msg_all(pokemon.key + " uses Paper on " + target.key + ". It's super effective!")

            elif pokedict[pokemon] == "scissors":
                if pokedict[target] == "rock":
                    self.msg_all(pokemon.key + " uses Scissors on " + target.key + ". It's not very effective!")

                else:
                    self.msg_all(pokemon.key + " uses Scissors on " + target.key + ". It's super effective!")


    #########################
    # Finish Script
    #########################

    def withdraw(self, trainer):
        trainer.msg("You have withdrawn from the battle.")
        self.msg_all(trainer.key + " has withdrawn from the battle.",
                     exceptions=[trainer])
        self.stop()

    def msg_all(self, message, exceptions=[]):
        "Send message to all combatants"
        for trainer in self.db.trainers.keys():
            if trainer not in exceptions:
                trainer.msg(message)

    def at_stop(self):
        """
        Called just before the script is stopped/destroyed.
        """
        for trainer in self.db.trainers.keys():
            # note: the list() call above disconnects list from database
            self._cleanup_trainer(trainer)
        for pokemon in self.db.pokemon.keys():
            # note: the list() call above disconnects list from database
            self._cleanup_pokemon(pokemon)

    def _cleanup_trainer(self, trainer):
        """
        Remove character from handler and clean
        it of the back-reference and cmdset
        """
        del trainer.ndb.combat_handler
        trainer.cmdset.delete("commands.battle.InBattleCmdSet")
        if self.db.phase == "Select":
            trainer.cmdset.delete("commands.battle.SelectCmdSet")
        elif self.db.phase == "Action":
            trainer.cmdset.delete("ActionCmdSet")

    def _cleanup_pokemon(self, pokemon):
        """
        Remove character from handler and clean
        it of the back-reference and cmdset
        """
        del pokemon.ndb.combat_handler

