"""
Object

The Object is the "naked" base class for things in the game world.

Note that the default Character, Room and Exit does not inherit from
this Object, but from their respective default implementations in the
evennia library. If you want to use this class as a parent to change
the other types, you can do so by adding this as a multiple
inheritance.

"""
from typeclasses.objects import Object

class Pokemon(Object):
    def at_object_creation(self):
        """This is called when object is first created."""
        super(Pokemon, self).at_object_creation()

        self.db.health = 10